#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"

#include "bg51/bg51.h"


void bg51_callback(uintptr_t context){
    bg51_interrupt_count *p = (bg51_interrupt_count *) context;
    ++(*p);
}

void timer_callback(TC_TIMER_STATUS status, uintptr_t context){
    bg51_timer_flag *p = (bg51_timer_flag *) context;
    *p = 1;
}

int main(void)
{
    SYS_Initialize(NULL);
    
    bg51_interrupt_count bg51_int_cnt = 0;
    EIC_CallbackRegister(EIC_PIN_12, bg51_callback, (uintptr_t) &bg51_int_cnt);
    
    bg51_timer_flag timer_flag = 0;
    TC3_TimerCallbackRegister(timer_callback, (uintptr_t) &timer_flag);
    TC3_TimerStart();
    
    
    bg51_t bg51;
    bg51_interrupt_count_register(&bg51, &bg51_int_cnt);
    bg51_timer_flag_register(&bg51, &timer_flag);
    bg51_init(&bg51, BG51_KOEFICIENT, 60);
    
    while(true)
    {
        SYS_Tasks();
        
        BG51_Tasks(&bg51);
        
        if(bg51_status_check(&bg51)){
            bg51_status_clear(&bg51);
            printf("radiation: %.03f uSv/h\r\n", bg51.data.radiation); 
        }
        
    }
    
    return ( EXIT_FAILURE );
}
