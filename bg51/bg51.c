/*
 * File:   bg51.c
 * Author: Miroslav Soukup
 * Description: Source file of bg51 radiation detector.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "bg51.h"


// *****************************************************************************
// Section: Functions
// *****************************************************************************

void bg51_init (bg51_t *me, float koeficient, bg51_time_period period) {
    me->state = BG51_TASKS_STATE_INIT;
    
    me->config.koeficient = koeficient;
    me->config.period_time = period;
}

uint8_t bg51_interrupt_count_register (bg51_t *me, bg51_interrupt_count *interrupt_count){
    if(!interrupt_count) return 0;
    me->config.int_cnt = interrupt_count;
    return 1;
}

uint8_t bg51_timer_flag_register (bg51_t *me, bg51_timer_flag *timer_flag){
    if(!timer_flag) return 0;
    me->config.timer_flag = timer_flag;
    return 1;
}

uint8_t bg51_status_check (bg51_t *me){
    return me->status.data_ready;
}

void bg51_status_clear (bg51_t *me){
    me->status.data_ready = 0;
}

void BG51_Tasks(bg51_t *me){
    
    switch(me->state){
        // INIT
        case BG51_TASKS_STATE_INIT:
            *(me->config.int_cnt) = 0;
            me->data.radiation    = 0;
            me->status.data_ready = 0;
            me->state = BG51_TASKS_STATE_WAIT;
        break;

        // WAIT
        case BG51_TASKS_STATE_WAIT:
            if(*(me->config.timer_flag)){
                *(me->config.timer_flag) = 0;
                me->state = 2;
            }
        break;

        // CALCULATE
        case BG51_TASKS_STATE_CALCULATE:
            me->data.radiation = (*(me->config.int_cnt) * (me->config.koeficient)) * (60.0 / (me->config.period_time));
            me->status.data_ready = 1;
            me->state = BG51_TASKS_STATE_CLEAR;
        break;
        
        // CLEAR
        case BG51_TASKS_STATE_CLEAR:
            *(me->config.int_cnt) = 0;
            me->state = BG51_TASKS_STATE_WAIT;
        break;
    }
    
}
