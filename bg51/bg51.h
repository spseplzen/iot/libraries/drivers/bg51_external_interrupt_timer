/*
 * File:   bg51.h
 * Author: Miroslav Soukup
 * Description: Header file of bg51 radiation detector.
 * 
 */



#ifndef BG51_H // Protection against multiple inclusion
#define BG51_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define BG51_KOEFICIENT     (0.2)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct bg51_descriptor bg51_t;
typedef struct bg51_data_descriptor bg51_data_t;
typedef struct bg51_config_descriptor bg51_config_t;
typedef struct bg51_status_descriptor bg51_status_t;

typedef volatile uint32_t bg51_interrupt_count;
typedef volatile uint8_t bg51_timer_flag;
typedef volatile uint32_t bg51_time_period;


typedef enum{
    BG51_TASKS_STATE_INIT      = 0,
    BG51_TASKS_STATE_WAIT      = 1,
    BG51_TASKS_STATE_CALCULATE = 2,
    BG51_TASKS_STATE_CLEAR     = 3
} BG51_TASKS_STATE_e;



struct bg51_status_descriptor{
    uint8_t data_ready;
};

struct bg51_config_descriptor{
    bg51_time_period period_time;
    bg51_timer_flag *timer_flag;
    bg51_interrupt_count *int_cnt;
    float koeficient;
};

struct bg51_data_descriptor{
    float radiation;
};

struct bg51_descriptor{
    BG51_TASKS_STATE_e state;
    
    bg51_config_t config;
    
    bg51_status_t status;
    
    bg51_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization bg51 module.
 *
 * \param me pointer to bg51 sensor type of bg51_t
 * \param koeficient for radiation calculation
 * \param period for radiation calculation
 *
 * \returns status of success / failure
 */
void bg51_init (bg51_t *me, float koeficient, bg51_time_period period);

uint8_t bg51_interrupt_count_register (bg51_t *me, bg51_interrupt_count *interrupt_count);

uint8_t bg51_timer_flag_register (bg51_t *me, bg51_timer_flag *timer_flag);

uint8_t bg51_status_check (bg51_t *me);

void bg51_status_clear (bg51_t *me);

void BG51_Tasks(bg51_t *me);




#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of BG51_H
